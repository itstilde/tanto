# *T.A.N.T.O*
The Acronym Never Thought Of
###### _its~_
______

##### Usage

Add this cookbook as a dependency in your `metadata.rb` file like this
```ruby
depends 'tanto'
```

or in your Berksfile like this
```ruby
# from bitbucket/github
cookbook 'tanto', git: 'git@bitbucket.org:jacob_mackenzie/tanto.git'
# or a local copy
cookbook 'tanto', path: '/path/to/tanto'
```



## OS
- Disable apt-daily service

### App Folder Structure
```
/app
/app/name
/app/name/current/       <- opsworks deploy area, equivilent of current
/app/name/releases/      <- rollback
/app/name/logs/          <- logs, definitely have to figure logging out
/app/name/storage/local/ <- database dumps / idk?
/app/name/storage/efs/   <- shared space across ec2 nodes stuff like easy greetings PHP files
/app/name/sessions/
/app/name/bin/           <- any binaries this application requires
/app/name/.himitsu/      <- secrets file for SSL certificate / API keys etc etc
```
### Tanto
```
/usr/tanto/             <- Global user - sudo rights
/usr/tanto/bin/
/usr/tanto/logs/
/usr/tanto/.himisu/
/usr/tanto/logs/        <- Logs for things i gu
```

## Laravel

#### Configure recipe
- set up user and directories and app config, optionally calls deploy

#### Deploy Recipe
- Deploys the latest version
- Check difference of composer.json? if no difference, copy the vendor
directory - look into shared vendor dir's!!

### Composer/ENV
  - CHECK FOR THE DIFFERENCES MAYBE A SHARED VENDOR DIRECTORY?
  - Write a depoyed application's state (build # etc etc) to .env file!
  - Tee composer log off to internal laravel's storage/logs/composer
  - write alias's for doing this without having to CD everwhere
  - or alternatively a gem file that fetches app states from .env/.state files?
  - Check that everything works after a composer install by somehow checking for http 200 on the URL?

#### Supervisor for local queues!

#### Lets Encrypt shit




### Platform Support

* ubuntu (>= 16.04)

### Cookbook Dependencies

* user
* poise-archive

### Attributes

Attribute | Description | Default | Choices
----------|-------------|---------|--------
`node['tanto']['name']` | `` | "tanto" |
`node['tanto']['group']` | `` | "tanto" |
`node['tanto']['home']` | `` | "/tanto" |
`node['tanto']['comment']` | `` | "The Acronym Never Thought Of" |
`node['tanto']['password']` | `` | "$1$loXWSlSU$EGgXOjr0iQITlPhy4PMkh1" |
`node['tanto']['shell']` | `` | "/bin/bash" |
`node['tanto']['sub_dirs']` | `` | "%w(bin log .himitsu)" |
`node['tanto']['dotfiles']['filename']` | `` | "dotfiles.zip" |
`node['tanto']['dotfiles']['url']` | `` | "https://bitbucket.org/jacob_mackenzie/haichi/get/master.zip" |

### Recipes

* tanto::default - Runs tanto::create
* [tanto::create](#tantocreate) - Creates our swiss army knife user

#### tanto::create

This recipe creates the tanto user/group and its subdirs

### Development and Testing

### Requirements

You will need [Ruby] with [Bundler].

[VirtualBox] and [Vagrant] are required
for integration testing with [Test Kitchen].

*Todo* Write a onliner for installing these via brew



### Rake

Run `rake list` for a list of commands


### Thor

Run `thor -T` to see all Thor tasks.



## License

MIT License

Copyright (c) 2016 its~

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


