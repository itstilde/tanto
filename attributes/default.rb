#
# Cookbook Name::tanto
# Attributes::default
#
# MIT License
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

default['tanto']['name'] = 'tanto'
default['tanto']['group'] = 'tanto'
default['tanto']['home'] = '/tanto'
default['tanto']['comment'] = 'The Acronym Never Thought Of'
default['tanto']['password'] = '$1$loXWSlSU$EGgXOjr0iQITlPhy4PMkh1'
default['tanto']['shell'] = '/bin/bash'
default['tanto']['sub_dirs'] = %w(bin log .himitsu)

default['tanto']['dotfiles']['filename'] = 'dotfiles.zip'
default['tanto']['dotfiles']['url'] = 'https://bitbucket.org/jacob_mackenzie/haichi/get/master.zip'

# TODO: Eventually make this pull from an S3 bucket
# default['tanto']['dotfiles']['s3_bucket'] = 'https://bitbucket.org/jacob_mackenzie/haichi/get/master.zip'
