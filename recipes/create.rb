=begin
#<
This recipe creates the tanto user/group and its subdirs
#>
=end

# rubocop:disable all
group "#{node['tanto']['group']}" do
  # rubocop:enable all
  action  :create
  not_if  "getent group #{node['tanto']['group']}"
end
# rubocop:disable all

user "#{node['tanto']['name']}" do
  # rubocop:enable all
  action      :create
  comment     node['tanto']['comment']
  gid         node['tanto']['group']
  home        node['tanto']['home']
  password    node['tanto']['password']
  shell       node['tanto']['shell']
  not_if      "getent passwd #{node['tanto']['name']}"
  manage_home true
end

directory node['tanto']['home'].to_s do
  action :create
  owner 'tanto'
  group 'tanto'
  mode '0755'
end

# unless ::File.directory?("#{node['tanto']['home']}")
node['tanto']['sub_dirs'].each do |dir|
  sub_dir = "#{node['tanto']['home']}/#{dir}"
  Chef::Log.debug "Creating sub dir #{sub_dir}"
  # rubocop:disable all
  directory "#{sub_dir}" do
    # rubocop:enable all
    action    :create
    owner     node['tanto']['name']
    group     node['tanto']['group']
    mode      '0750'
    # rubocop:disable all
    not_if { ::File.directory?("#{sub_dir}") }
    # rubocop:enable all
  end
end
