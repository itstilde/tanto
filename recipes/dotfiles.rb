=begin
#<
This recipe downloads & installs the users dotfiles
#>
=end

remote_file "#{Chef::Config['file_cache_path']}/#{node['tanto']['dotfiles']['filename']}" do
  source node['tanto']['dotfiles']['url']
end

poise_archive "#{Chef::Config['file_cache_path']}/#{node['tanto']['dotfiles']['filename']}" do
  destination node['tanto']['home']
  strip_components 1
  action :unpack
  group 'tanto'
  user 'tanto'
end
