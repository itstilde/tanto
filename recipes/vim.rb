=begin
#<
This recipe sets up vim
#>
=end
# instal

bash 'install dein plugin manager' do
  cwd "#{node['tanto']['home']}/.vim"
  code 'sh ./installer.sh .'
  not_if { ::File.directory?("#{node['tanto']['home']}/.vim/repos") }
end
