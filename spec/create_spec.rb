require 'spec_helper'
require 'json'
attributes = ::JSON.parse(::IO.read('./test/attributes.json'))

describe 'tanto::create' do
  # let(:chef_run) { ChefSpec::ServerRunner.new.converge(described_recipe) }
  let(:chef_run) { ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe) }

  before do
    # As these don't run on a regular box we'll need to mock them
    stub_command('getent group tanto').and_return(false)
    stub_command('getent passwd tanto').and_return(false)
  end

  it 'creates a group named tanto' do
    expect(chef_run).to create_group(attributes['tanto']['group'])
  end

  it 'creates a user named tanto' do
    expect(chef_run).to create_user(attributes['tanto']['name'])
  end

  it 'creates the home dir' do
    expect(chef_run).to create_directory(attributes['tanto']['home'])
  end

  it 'creates the bin subdir' do
    expect(chef_run).to create_directory("#{attributes['tanto']['home']}/bin")
  end

  it 'creates the log subdir' do
    expect(chef_run).to create_directory("#{attributes['tanto']['home']}/log")
  end
  it 'creates the himitsu subdir' do
    expect(chef_run).to create_directory("#{attributes['tanto']['home']}/.himitsu")
  end
end
