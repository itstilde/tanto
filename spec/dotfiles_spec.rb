require 'spec_helper'
require 'json'
attributes = ::JSON.parse(::IO.read('./test/attributes.json'))

describe 'tanto::dotfiles' do
  # let(:chef_run) { ChefSpec::ServerRunner.new.converge(described_recipe) }
  let(:chef_run) { ChefSpec::SoloRunner.new(platform: 'ubuntu', version: '16.04').converge(described_recipe) }

  it 'saves the latest dotfiles zip file' do
    expect(chef_run).to create_remote_file("#{Chef::Config['file_cache_path']}/#{attributes['tanto']['dotfiles']['filename']}")
  end

  it 'extracts dotfiles.zip into ~/' do
    expect(chef_run).to unpack_poise_archive("#{Chef::Config['file_cache_path']}/#{attributes['tanto']['dotfiles']['filename']}").with(absolute_path: "#{Chef::Config['file_cache_path']}/#{attributes['tanto']['dotfiles']['filename']}", destination: attributes['tanto']['home'])
  end
end
