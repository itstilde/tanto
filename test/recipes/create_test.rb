# # encoding: utf-8

# Inspec test for recipe tanto::create

# The Inspec reference, with examples and extensive documentation, can be
# found at https://docs.chef.io/inspec_reference.html

require 'json'
attributes = ::JSON.parse(::IO.read('./test/attributes.json'))

describe group(attributes['tanto']['group']) do
  it { should exist }
end

describe user(attributes['tanto']['name']) do
  it { should exist }
end

describe directory(attributes['tanto']['home']) do
  it { should exist }
end

describe directory("#{attributes['tanto']['home']}/bin") do
  it { should exist }
end
describe directory("#{attributes['tanto']['home']}/log") do
  it { should exist }
end

describe directory("#{attributes['tanto']['home']}/.himitsu") do
  it { should exist }
end
